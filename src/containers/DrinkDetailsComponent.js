import React from "react";
import Nav from "react-bootstrap/Nav";

const DrinkDetailsContainer = (props) => {
  const { details } = props;
  return (
    <>
      {details.strDrinkThumb && (
        <img src={details.strDrinkThumb} alt={details.strDrink} />
      )}
      {Object.keys(details)
        .filter(
          (k) => k.indexOf("Ingredient") === -1 && k.indexOf("Measure") === -1
        )
        .filter((k) => !["DrinkThumb", "strDrink", "idDrink"].includes(k))
        .map(
          (k) =>
            details[k] &&
            details[k] && (
              <span className={"margin-top-5"} key={`details${k}`}>{`${k.slice(
                3,
                k.length
              )}: ${details[k]}`}</span>
            )
        )}
      <span className={"margin-top-20"}>Ingredients:</span>
      {Object.keys(details)
        .filter((k) => k.indexOf("Ingredient") !== -1)
        .map(
          (k, index) =>
            details[k] && (
              <Nav.Link key={`data${k}`} href={`/ingredients/${details[k]}`}>
                {`${details[`strMeasure${index + 1}`]} of ${details[k]}`}
              </Nav.Link>
            )
        )}
    </>
  );
};

export default DrinkDetailsContainer;

import React from "react";
import { LinkContainer } from "react-router-bootstrap";
import { withRouter } from "react-router-dom";

import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

const Header = () => (
  <Navbar bg="dark" variant="dark" expand="lg" className="mb-5">
    <Navbar.Brand href="/">React-Bootstrap</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <LinkContainer exact={true} to="/">
          <Nav.Link>Home</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/randomdrink">
          <Nav.Link>Random</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/letter">
          <Nav.Link>Letter</Nav.Link>
        </LinkContainer>
        <LinkContainer exact={true} to="/ingredients">
          <Nav.Link>Ingredients</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/todo">
          <Nav.Link>Todo</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/exercise">
          <Nav.Link>Exercise</Nav.Link>
        </LinkContainer>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

export default withRouter(Header);

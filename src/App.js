import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Container from "react-bootstrap/Container";

import Home from "./pages/Home";
import Todo from "./pages/Todo";
import RandomDrink from "./pages/RandomDrink";
import Letter from "./pages/Letter";
import DrinkDetails from "./pages/DrinkDetails";
import Exercise from "./pages/Exercise";
import Ingredient from "./pages/Ingredient";
import Category from "./pages/Category";
import IngredientList from "./pages/IngredientList";
import Header from "./components/Header";

function App() {
  return (
    <BrowserRouter>
      <Container className="p-3">
        <Header />
        <Switch>
          <Route path="/letter">
            <Letter />
          </Route>
          <Route path="/randomdrink">
            <RandomDrink />
          </Route>
          <Route path="/todo">
            <Todo />
          </Route>
          <Route path="/drink/:id">
            <DrinkDetails />
          </Route>
          <Route path="/ingredients/:name">
            <Ingredient />
          </Route>
          <Route path="/category/:name">
            <Category />
          </Route>
          <Route path="/ingredients">
            <IngredientList />
          </Route>
          <Route path="/exercise">
            <Exercise />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;

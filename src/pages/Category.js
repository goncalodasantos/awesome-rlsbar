import React, { useState, useEffect } from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Table from "react-bootstrap/Table";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";

import { useParams } from "react-router-dom";

import { getDrinkInCategory } from "../api";

const Category = () => {
  const [loading, setLoading] = useState(false);
  const [details, setDetails] = useState(null);
  const [inverseOrder, setInverseOrder] = useState(false);
  const { name } = useParams();

  useEffect(() => {
    async function getData(name) {
      setLoading(true);
      const detailsResult = await getDrinkInCategory(name.replaceAll("-", "/"));
      if (detailsResult && detailsResult.length > 0) {
        setDetails(detailsResult);
      } else {
        setDetails(null);
      }
      setLoading(false);
    }
    getData(name);
  }, [name]);

  return (
    <>
      <Row>
        <Col sm={12}>
          <Jumbotron>
            <h1 className="header">RedLight Category</h1>
          </Jumbotron>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <h1 className="header">{`${name.replaceAll("-", "/")}`}</h1>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <Nav className="flex-column">
            {loading && (
              <Spinner animation="border" role="status" className="mx-auto">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
            {!loading && details && (
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Picture</th>
                    <th
                      className={"clickable"}
                      onClick={() => setInverseOrder(!inverseOrder)}
                    >
                      Name {inverseOrder ? "↓" : "↑"}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {details
                    .sort((a, b) =>
                      inverseOrder
                        ? b.strDrink.localeCompare(a.strDrink)
                        : a.strDrink.localeCompare(b.strDrink)
                    )
                    .map((value) => (
                      <tr key={`drinkName${value.strDrink}`}>
                        <td>
                          {value.strDrinkThumb && (
                            <img
                              src={value.strDrinkThumb}
                              alt={value.strDrink}
                            />
                          )}
                        </td>
                        <td>
                          <Nav.Link
                            key={`ing${value.idDrink}`}
                            href={`/drink/${value.idDrink}`}
                          >
                            {`${value.strDrink}`}
                          </Nav.Link>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            )}

            {!loading && !details && <span>No ingredient with that name</span>}
          </Nav>
        </Col>
      </Row>
    </>
  );
};

export default Category;

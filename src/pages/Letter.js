import React from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";
import Nav from "react-bootstrap/Nav";
import Table from "react-bootstrap/Table";

import { getDrinksByLetter } from "../api";

class Letter extends React.Component {
  state = {
    loading: false,
    cocktails: [],
    listOfLetters: new Array(26)
      .fill(1)
      .map((_, i) => String.fromCharCode(65 + i)),
    drinkList: null,
    selectedLetter: null,
    inverseOrder: false,
  };
  async changeSelectedAndFetch(letter) {
    this.setState({ ...this.state, selectedLetter: letter, loading: true });
    const drinkList = await getDrinksByLetter(letter);
    this.setState({ loading: false, drinkList: drinkList });
  }

  componentDidMount() {
    this.changeSelectedAndFetch("A");
  }

  render() {
    const {
      loading,
      drinkList,
      listOfLetters,
      selectedLetter,
      inverseOrder,
    } = this.state;

    return (
      <>
        <Row>
          <Col sm={12}>
            <Jumbotron>
              <h1 className="header">Filter by letter</h1>
            </Jumbotron>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Container>
              <Row>
                {listOfLetters.map((el) => (
                  <Col
                    key={`letterKey${el}`}
                    onClick={() =>
                      selectedLetter !== el && this.changeSelectedAndFetch(el)
                    }
                    className={"clickable"}
                  >
                    <span
                      className={selectedLetter === el ? "selectedLetter" : ""}
                    >
                      {el}
                    </span>
                  </Col>
                ))}
              </Row>
              <Row className={"margin-top-20"}>
                <Col sm={12}>
                  <Nav className="flex-column">
                    {loading && (
                      <Spinner
                        animation="border"
                        role="status"
                        className="mx-auto"
                      >
                        <span className="sr-only">Loading...</span>
                      </Spinner>
                    )}
                    {!loading && (!drinkList || drinkList.length) && (
                      <span>No drinks starting with that letter</span>
                    )}

                    {!loading && drinkList && (
                      <Table striped bordered hover>
                        <thead>
                          <tr>
                            <th>Picture</th>
                            <th
                              className={"clickable"}
                              onClick={() =>
                                this.setState({
                                  ...this.state,
                                  inverseOrder: !inverseOrder,
                                })
                              }
                            >
                              Name {inverseOrder ? "↓" : "↑"}
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {drinkList
                            .sort((a, b) =>
                              inverseOrder
                                ? b.strDrink.localeCompare(a.strDrink)
                                : a.strDrink.localeCompare(b.strDrink)
                            )
                            .map((el) => (
                              <tr key={`drinkName${el.strDrink}`}>
                                <td>
                                  {el.strDrinkThumb && (
                                    <img
                                      src={el.strDrinkThumb}
                                      alt={el.strDrink}
                                    />
                                  )}
                                </td>
                                <td>
                                  <Nav.Link
                                    key={`ing${el.idDrink}`}
                                    href={`/drink/${el.idDrink}`}
                                  >
                                    {`${el.strDrink}`}
                                  </Nav.Link>
                                </td>
                              </tr>
                            ))}
                        </tbody>
                      </Table>
                    )}
                  </Nav>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </>
    );
  }
}

export default Letter;

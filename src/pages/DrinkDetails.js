import React, { useState, useEffect } from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";
import DrinkDetailsContainer from "../containers/DrinkDetailsComponent";
import { useParams } from "react-router-dom";

import { getDrinkById } from "../api";

const DrinkDetails = () => {
  const [loading, setLoading] = useState(false);
  const [details, setDetails] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    async function getData(id) {
      setLoading(true);
      const detailsResult = await getDrinkById(id);
      console.log(detailsResult);
      if (detailsResult && detailsResult.length > 0) {
        setDetails(detailsResult[0]);
      } else {
        setDetails(null);
      }
      setLoading(false);
    }
    getData(id);
  }, [id]);

  return (
    <>
      <Row>
        <Col sm={12}>
          <Jumbotron>
            <h1 className="header">RedLight Bar</h1>
          </Jumbotron>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <h1 className="header">{details && details.strDrink}</h1>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <Nav className="flex-column">
            {loading && (
              <Spinner animation="border" role="status" className="mx-auto">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
            {!loading && details && <DrinkDetailsContainer details={details} />}
            {!loading && !details && <span>No drink with that id</span>}
          </Nav>
        </Col>
      </Row>
    </>
  );
};

export default DrinkDetails;

import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Nav from "react-bootstrap/Nav";

const Exercise = () => {
  return (
    <Row>
      <Col sm={12}>
        <Nav className="flex-column">
          <Row className={"margin-top-20"}>Home</Row>
          <Row>
            When you click a category, redirect to /category/*name* - watch out
            for the slashes
          </Row>
          <Row className={"margin-top-20"}>Random</Row>
          <Row>
            Get a random drink, use DrinkDetailsComponent to render the details
            if it exists
          </Row>
          <Row className={"margin-top-20"}>Letter</Row>
          <Row>
            Pick a letter, send the request to fetch it and present the results
            in the page. When you click the name of a drink, redirect to
            /category/*name*
          </Row>
          <Row className={"margin-top-20"}>Ingredients</Row>
          <Row>
            Filter using the debounced input. Update a different list in state
            just for clarity (we could do in sito, as well, but is clearer that
            way). When searching, convert to lowercase if necessary (done by
            choice)
          </Row>
          <Row className={"margin-top-20"}>Ingredient page</Row>
          <Row>
            Since we have don't have ids for ingredients available at drink
            level, let's use the name as match. Fetch details, add the details
            that are available, take out the less user friendly ones (like id)
          </Row>

          <Row className={"margin-top-20"}>Drink details page</Row>
          <Row>
            Use id as match; show the details using DrinkDetailsComponent
          </Row>

          <Row className={"margin-top-20"}>DrinkDetailsComponent</Row>
          <Row>
            Presents cool (and existent) properties to the user about the drink
            passed by props. Shows ingredients as links, with their measures
            parsed as well.
          </Row>

          <Row className={"margin-top-20"}>Other stuff</Row>
          <Row>
            We're using nav.links to navigate, but I usually use history
            directly (or navigation in RN) to do so.
          </Row>

          <Row className={"margin-top-20"}>
            What I would do if you had unlimited time to review this:
          </Row>
          <Row>
            <ul>
              <li>
                precommit hook to prevent styling errors from being on git +
                commitlint + semantic-release
              </li>
              <li>
                add editor-config - just prevents discussions about spacing
              </li>
              <li>use TS</li>
              <li>Add, in general, a bunch of CSS</li>
              <li>
                separate this in folders containing index.tsx + styles.css
              </li>
              <li>move the App.js to the containers folder</li>
              <li>
                add a couple of folders for contexts, types, api requests
                serated by responsability
              </li>
              <li>
                separate this in folders containing index.tsx + styles.css
              </li>
              <li>If possible, use styled-components</li>
              <li>Add tests :) :) :) :)</li>
              <li>
                Make the search in ingredients list reflect on the router after
                the debounce, to make the user able to send it to another fellow
                drinker
              </li>
              <li>Configure jsconfig to use absolute paths</li>
              <li>
                Get a better styling framework or build most of the components
                by myself
              </li>
              <li>
                Move routes to a different file for maintainability. I also like
                to prevent routes unacessible from being injected into the
                router, based on each route allowed roles
              </li>
              <li>Maybe get this on on netlify or something for you to see?</li>
            </ul>
          </Row>

          <Row className={"margin-top-20"}>
            I figured you're probably going to check this at 8pm in a friday, so
            why mess with your weekend. Hit me on goncalodasantos@gmail.com if
            you want to scream at me or something
          </Row>
        </Nav>
      </Col>
    </Row>
  );
};

export default Exercise;

import React, { useState, useEffect } from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";

import { useParams } from "react-router-dom";

import { getIngredientByName } from "../api";

const Ingredient = () => {
  const [loading, setLoading] = useState(false);
  const [details, setDetails] = useState(null);
  const { name } = useParams();

  useEffect(() => {
    async function getData(name) {
      setLoading(true);
      const detailsResult = await getIngredientByName(name);
      if (detailsResult && detailsResult.length > 0) {
        setDetails(detailsResult[0]);
      } else {
        setDetails(null);
      }
      setLoading(false);
    }
    getData(name);
  }, [name]);

  return (
    <>
      <Row>
        <Col sm={12}>
          <Jumbotron>
            <h1 className="header">RedLight Bar</h1>
          </Jumbotron>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <h1 className="header">{`${name}`}</h1>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <Nav className="flex-column">
            {loading && (
              <Spinner animation="border" role="status" className="mx-auto">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
            {!loading && details && (
              <>
                {details.strDrinkThumb && (
                  <img src={details.strDrinkThumb} alt={name} />
                )}
                {Object.keys(details)
                  .filter((k) => k.indexOf("idDrink") === -1)
                  .filter((k) => !["strIngredient", "idIngredient"].includes(k))
                  .map(
                    (k) =>
                      details[k] &&
                      details[k] && (
                        <span
                          className={"margin-top-5"}
                          key={`ingredientStuff${k}`}
                        >{`${k.slice(3, k.length)}: ${details[k]}`}</span>
                      )
                  )}
              </>
            )}
            {!loading && !details && <span>No ingredient with that name</span>}
          </Nav>
        </Col>
      </Row>
    </>
  );
};

export default Ingredient;

import React from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";
import Nav from "react-bootstrap/Nav";
import DrinkDetailsContainer from "../containers/DrinkDetailsComponent";

import { getRandomDrink } from "../api";

class RandomDrink extends React.Component {
  state = {
    loading: false,
    cocktail: null,
    drink: null,
  };

  async getRandom() {
    this.setState({ loading: true });
    const randDrink = await getRandomDrink();
    this.setState({ loading: false, cocktail: randDrink });
  }

  async componentDidMount() {
    await this.getRandom();
  }

  render() {
    const { loading, cocktail } = this.state;
    return (
      <>
        <Row>
          <Col sm={9}>
            <Jumbotron>
              <h1 className="header">Random Drink</h1>
            </Jumbotron>
          </Col>

          <Col sm={3}>
            <Button onClick={() => this.getRandom()}>
              <h2 className="random-button">Random Drink</h2>
            </Button>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <h1 className="header">{cocktail && cocktail.strDrink}</h1>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <Nav className="flex-column">
              {loading && (
                <Spinner animation="border" role="status" className="mx-auto">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              )}
              {!loading && cocktail && (
                <DrinkDetailsContainer details={cocktail} />
              )}

              {!loading && !cocktail && <span>No drink with that id</span>}
            </Nav>
          </Col>
        </Row>
      </>
    );
  }
}

export default RandomDrink;

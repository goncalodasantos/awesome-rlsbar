import React, { useState, useEffect } from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";

import { getCategories } from "../api";

const DrinkDetails = (props) => {
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    async function getData() {
      setLoading(true);
      const cats = await getCategories();
      setCategories(cats);
      setLoading(false);
    }
    getData();
  }, []);

  return (
    <>
      <Row>
        <Col sm={12}>
          <Jumbotron>
            <h1 className="header">RedLight Bar</h1>
          </Jumbotron>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <h1 className="header">Categories</h1>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <Nav className="flex-column">
            {loading && (
              <Spinner animation="border" role="status" className="mx-auto">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
            {categories.map((c) => (
              <Nav.Link key={c} href={`/category/${c.replaceAll("/", "-")}`}>
                {c}
              </Nav.Link>
            ))}
          </Nav>
        </Col>
      </Row>
    </>
  );
};

export default DrinkDetails;

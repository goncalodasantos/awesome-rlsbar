import React, { useState, useEffect } from "react";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";
import Jumbotron from "react-bootstrap/Jumbotron";

import { getIngredients } from "../api";

const IngredientList = () => {
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");
  const [searchAfterDebounce, setSearchAfterDebounce] = useState("");
  const [ingredientsList, setIngredientsList] = useState(null);
  const [ingredientsListFiltered, setIngredientsListFiltered] = useState(null);

  useEffect(() => {
    async function getData() {
      setLoading(true);
      const ingredientsListResult = await getIngredients();
      if (ingredientsListResult && ingredientsListResult.length > 0) {
        setIngredientsList(ingredientsListResult);
        setIngredientsListFiltered(ingredientsListResult);
      } else {
        setIngredientsList(null);
        setIngredientsListFiltered(null);
      }
      setLoading(false);
    }
    getData();
  }, []);

  // debounce the update so we can write a long string in the input without the cpu thermal throttling. Thanks Apple
  useEffect(() => {
    const timeout = setTimeout(() => {
      setIngredientsListFiltered(
        ingredientsList
          ? ingredientsList.filter(
              (el) => el.toLowerCase().indexOf(search.toLowerCase()) !== -1
            )
          : null
      );
      setSearchAfterDebounce(search);
    }, 400);
    return () => clearTimeout(timeout);
  }, [search, ingredientsList]);

  return (
    <>
      <Row>
        <Col sm={9}>
          <Jumbotron>
            <h1 className="header">
              {searchAfterDebounce === ""
                ? `All ingredients`
                : `Ingredients search for ${searchAfterDebounce}`}
            </h1>
          </Jumbotron>
        </Col>
        <Col sm={3}>
          <Row>
            <h3>Search</h3>
          </Row>
          <Row>
            <input onChange={(e) => setSearch(e.target.value)} />
          </Row>
        </Col>
      </Row>
      <Row>
        <Col sm={12}>
          <Nav className="flex-column">
            {loading && (
              <Spinner animation="border" role="status" className="mx-auto">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
            {!loading && ingredientsListFiltered && (
              <>
                {ingredientsListFiltered.map((value, index) => (
                  <Nav.Link
                    key={`ing${value}${index}`}
                    href={`/ingredients/${value}`}
                  >
                    {`${value}`}
                  </Nav.Link>
                ))}
              </>
            )}
            {!loading && !ingredientsListFiltered && (
              <span>No ingredient with that name</span>
            )}
          </Nav>
        </Col>
      </Row>
    </>
  );
};

export default IngredientList;
